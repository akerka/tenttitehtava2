﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace Tentti_tehtava_2
{
    class Program
    {
        

        static void Main(string[] args)
        {
            Piste a = new Piste("A", 1, 2);
            Piste b = new Piste("B", 2, 3);
            Piste c = new Piste("C", 3, 4);
            Piste d = new Piste("D", 4, 5);

            string jsonPisteet = "";

            List<Piste> pisteet = new List<Piste>();

            pisteet.Add(a);
            pisteet.Add(b);
            pisteet.Add(c);
            pisteet.Add(d);


            jsonPisteet = Serialize(pisteet);

            Console.WriteLine(jsonPisteet);
            Console.ReadKey();

            ReadBinaryToList(pisteet);
            Console.ReadKey();

            jsonPisteet = Serialize(pisteet);

            Console.WriteLine(jsonPisteet);
            Console.ReadKey();

        }

        public static string Serialize(List<Piste> pisteet)
        {
            string json = "";
            json = JsonConvert.SerializeObject(pisteet);
            return json;
        }

        public static void ReadBinaryToList(List<Piste> pisteet)
        {
            FileStream stream = new FileStream("C:\\tmp\\bindataTentti.bin",
                FileMode.Open);

            BinaryReader reader = new BinaryReader(stream);

            reader.BaseStream.Seek(0, SeekOrigin.Begin);

            while (reader != null && reader.BaseStream.Position < reader.BaseStream.Length)
            {
                string nimi = reader.ReadString();
                double x = reader.ReadDouble();
                double y = reader.ReadDouble();

                pisteet.Add(new Piste(nimi, x, y));

                Console.WriteLine(nimi);
                Console.WriteLine(x);
                Console.WriteLine(y);
            }
        }
    }
}
